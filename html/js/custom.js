jQuery(document).ready(function() {
  jQuery('[data-toggle="tooltip"]').tooltip();

  jQuery('.green-box ').append("<span class='lines top'></span><span class='lines right'></span><span class='lines bottom'></span><span class='lines left'></span><span class='lines TopRight'></span><span class='lines Bottomleft'></span>");

  jQuery('.gray-box ').append("<span class='lines top'></span><span class='lines right'></span><span class='lines bottom'></span><span class='lines left'></span><span class='lines TopRight'></span>");
});

jQuery(window).on("load resize scroll", function(e) {
  var Win = jQuery(window).height();
  var Header = jQuery("header").outerHeight();
  var Footer = jQuery("footer").outerHeight();

  var NHF = Header + Footer;

  jQuery('.section').css('min-height', (Win - NHF));

});

jQuery(".game-select ul li a").click(function() {
  jQuery(this).parent().css('opacity', 1).siblings().css('opacity', 0.3);

});

jQuery(".LocationVenueList ul li a").click(function() {
  jQuery(this).parent().addClass("active").siblings().removeClass("active");

});


$(function() {
    //----- OPEN
    $('[pd-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('pd-popup-open');
        jQuery("body").addClass("open");
        $('[pd-popup="' + targeted_popup_class + '"]').fadeIn(100);

        e.preventDefault();
    });

    //----- CLOSE
    $('[pd-popup-close]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('pd-popup-close');
        jQuery("body").removeClass("open");
        $('[pd-popup="' + targeted_popup_class + '"]').fadeOut(200);

        e.preventDefault();
    });
});



document.documentElement.addEventListener('touchstart', function(event) {
  if (event.touches.length > 1) {
    event.preventDefault();
  }
}, false);
